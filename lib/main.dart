import 'package:flutter/material.dart';

enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme{
  ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.lightGreenAccent,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        )
    );
  }
  ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.lightGreenAccent,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.amberAccent,
        )
    );
  }
}
class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
        ? MyAppTheme().appThemeLight()
        : MyAppTheme().appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
        // body: Container(
        //   color: Colors.cyan[50],
        // ),

      ),
    );

  }

  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.call,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Call"),
      ],
    );
  }

  Widget buildTextButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.textsms_outlined,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Text"),
      ],
    );
  }

  Widget buildVideoCallButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.video_camera_front,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Video"),
      ],
    );
  }

  Widget buildEmailButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.email_outlined,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Email"),
      ],
    );
  }

  Widget buildDirectionsButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.directions,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Directions"),
      ],
    );
  }

  Widget buildPayButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.attach_money,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Pay"),
      ],
    );
  }

  Widget mobilePhoneListTile(){
    return ListTile(
      leading: Icon(Icons.call),
      title: Text("330-803-3390"),
      subtitle: Text("mobile"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        color: Colors.amber,
        onPressed: (){},
      ),
    );
  }

  Widget otherPhoneListTile(){
    return ListTile(
      leading: Text(" "),
      title: Text("440-440-3390"),
      subtitle: Text("other"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        color: Colors.amber,
        onPressed: (){},
      ),
    );
  }
Widget emailListTile(){
    return ListTile(
      leading: Icon(Icons.email),
      title: Text("priyanka@priyanka.com"),
      subtitle: Text("work"),
    );
  }
  Widget addressListTile(){
    return ListTile(
        leading: Icon(Icons.location_on),
        title: Text("234 Sunset St, Burlingame"),
        subtitle: Text("home"),
        trailing: IconButton(
        icon: Icon(Icons.directions),
        color: Colors.amber,
        onPressed: (){},
      ),
    );
  }

  AppBar buildAppBarWidget(){
    return AppBar(
      backgroundColor: Colors.cyan[200],
      leading: Icon(
        Icons.arrow_back,
        color: Colors.blue,
      ),
      // title: Text("Hello Flutter App"),
      actions: <Widget>[
        IconButton(
            onPressed: (){},
            icon: Icon(
              Icons.star_border_outlined,
              color: Colors.blue,
            )
        ),
      ],
    );
  }

  Widget buildBodyWidget(){
    return ListView (
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              // color: Colors.cyan,
              width: double.infinity,
              //Height constraint at Container widget level
              height: 250,
              child: Image.network(
                "https://scontent.fbkk10-1.fna.fbcdn.net/v/t39.30808-6/240525698_4406131906120544_1431153781683796918_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=174925&_nc_eui2=AeEWy35qY6p-s4qPPNXxPYYHufLvxaoQ0CK58u_FqhDQIksrtywp8J04Oca_gkIB6vdoNCYX-7AoNR3_YTorgkjP&_nc_ohc=oOwOYve4dCsAX8zZLuA&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfAgFtlqeOs8SqEI3N9BH6uAeChtAVcd6z5qEdQUkpzy3w&oe=63A72D10",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(8.0),
                    child: Text("SANia",
                      style: TextStyle(fontSize: 30),
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.blueGrey[600],
            ),
            Container(
              margin: const EdgeInsets.only(top: 8,bottom: 8),
              child: Theme(
                data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.pinkAccent,
                  ),
                ),
                child: profileActionTtems(),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),

            ),mobilePhoneListTile(),
            otherPhoneListTile(),
            emailListTile(),
            addressListTile(),
          ],
        ),
      ],
    );
  }

  Widget profileActionTtems(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildCallButton(),
        buildTextButton(),
        buildVideoCallButton(),
        buildEmailButton(),
        buildDirectionsButton(),
        buildPayButton(),
      ],
    );
  }
}
